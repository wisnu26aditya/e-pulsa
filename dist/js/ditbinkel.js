$(document).ready(function(){
    function loadMyLoading(){

		var background = '<div class="lb_background"></div>';
		var canvas ='<div class="lb_canvas"><span><img src="dist/img/ajax-loader.gif" /> waiting for sync</span></div>';
		var ttl_loading = background+canvas;
		
		if($(".lb_background").length< 1){
			$(ttl_loading).appendTo("body").fadeIn("slow");
		}
		
		wc = $(".lb_canvas").outerWidth();
		lc = ($(window).width() - wc ) / 2;
		
		$(".lb_canvas").css("left",lc);
		
	};	
	
	function closeMyLoading(){
		$(".lb_background, .lb_canvas").fadeOut("slow",function(){
			$(".lb_background, .lb_canvas").remove()
		});
		
	}

     

    $(document).on("click","#syncrekapkehadiran",function(event){
        event.preventDefault();
        loadMyLoading();
        var $this = $(this), url = $this.attr("href");
        
        $.ajax({
            type : "GET",
            cache : false,
            url : url,
            success : function(data){
                
                   window.location.href = "dashboard.php?cat=hr&page=rekapkehadiran";
					closeMyLoading();
                    
            },
            error : function(){
                closeMyLoading();
                alert("fail");
            }
        })
    });

    $(document).on("click","#lockrekapkehadiran",function(event){
        event.preventDefault();
        loadMyLoading();
        var $this = $(this), url = $this.attr("href");
        
        $.ajax({
            type : "GET",
            cache : false,
            url : url,
            success : function(data){
                
                   window.location.href = "dashboard.php?cat=hr&page=rekapkehadiran";
					closeMyLoading();
                    
            },
            error : function(){
                closeMyLoading();
                alert("fail");
            }
        })
    });

     $(document).on("click","#syncrekapitulasi",function(event){
        event.preventDefault();
        loadMyLoading();
        var $this = $(this), url = $this.attr("href");
        
        $.ajax({
            type : "GET",
            cache : false,
            url : url,
            success : function(data){
                
                   window.location.href = "dashboard.php?cat=hr&page=rekapitulasiabsensi";
					closeMyLoading();
                    
            },
            error : function(){
                closeMyLoading();
                alert("fail");
            }
        })
    });

    $(document).on("click","#lockrekapitulasi",function(event){
        event.preventDefault();
        loadMyLoading();
        var $this = $(this), url = $this.attr("href");
        
        $.ajax({
            type : "GET",
            cache : false,
            url : url,
            success : function(data){
                
                   window.location.href = "dashboard.php?cat=hr&page=rekapitulasiabsensi";
					closeMyLoading();
                    
            },
            error : function(){
                closeMyLoading();
                alert("fail");
            }
        })
    });

     $(document).on("click","#syncpembtukin",function(event){
        event.preventDefault();
        loadMyLoading();
        var $this = $(this), url = $this.attr("href");
        console.log("tes");
        $.ajax({
            type : "GET",
            cache : false,
            url : url,
            success : function(data){
                
                   window.location.href = "dashboard.php?cat=hr&page=pembayarantukin";
					closeMyLoading();
                    
            },
            error : function(){
                closeMyLoading();
                alert("fail");
            }
        })
    });

    $(document).on("click","#lockpembtukin",function(event){
        event.preventDefault();
        loadMyLoading();
        var $this = $(this), url = $this.attr("href");
        
        $.ajax({
            type : "GET",
            cache : false,
            url : url,
            success : function(data){
                
                   window.location.href = "dashboard.php?cat=hr&page=pembayarantukin";
					closeMyLoading();
                    
            },
            error : function(){
                closeMyLoading();
                alert("fail");
            }
        })
    });

    
     
});