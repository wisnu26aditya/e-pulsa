<link rel="stylesheet" href="dist/css/bootstrap.min.css" type="text/css" />
<style type="text/css">

.dropdown-submenu {
    position: relative;
}

.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}
</style>
<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href=""><img alt="" src="" height="150%"></a>
	   </div>
	   <div>
	   <ul class="nav navbar-nav navbar-right">
        <li><a href="#"></a></li>
		<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		  <font color='#66FFFF'><?php echo "Hi, ".$_SESSION['username'];?></font> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="dashboard.php?cat=web&page=chgpwd"><span class="icon-wrench"></span> Ubah Password</a></li>
            <li role="separator" class="divider"></li>
             <li><a href="dashboard.php?cat=web&page=logout"><span class="icon-off"></span> Keluar</a></li>
          </ul>
		  </li>
		  </li>
		  </ul>	
	</div>
	
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="dashboard.php">Home<span class="sr-only">(current)</span></a></li>
		 <!--MENU HRD-->
		 <?php
		if($_SESSION['login_hash']=="hr")
		{
		?>
	<li class="dropdown">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Data Admin<span class="caret"></span></a>
			<ul class="dropdown-menu">	
				<!-- Untuk data pulsa -->
				<li class="dropdown-submenu">
					<a href="#">Data Transaksi</a>
						<ul class="dropdown-menu">
							<li><a href="?cat=hr&page=view">View Transaksi</a></li>
						</ul>
				</li>
 
	<!--MENU ADMIN-->
        <?php
	}elseif($_SESSION['login_hash']=="administrator"){
	?>    	
		
	<li class="dropdown">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Data Admin<span class="caret"></span></a>
			<ul class="dropdown-menu">	
				<!-- Untuk data admin -->
				<li class="dropdown-submenu">
					<a href="#">User</a>
						<ul class="dropdown-menu">
							<li><a href="?cat=administrator&page=user">Tambah User</a></li> 	
						</ul>
				</li>
			
				<!-- Untuk data pulsa -->
				<li class="dropdown-submenu">
					<a href="#">Data Transaksi</a>
						<ul class="dropdown-menu">
							<li><a href="?cat=administrator&page=transaksi">Transaksi E-Pulsa</a></li>
							<li><a href="?cat=administrator&page=view">View Transaksi</a></li>
						</ul>
				</li>
				
    
  	<?php
	}
	?>	
		</ul>
<!--     
	 <ul class="nav navbar-nav navbar-right">
        <li><a href="#"></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		  <?php echo "Hi, ".$_SESSION['username'];?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="dashboard.php?cat=web&page=chgpwd"><span class="icon-wrench"></span> Ubah Password</a></li>
            <li role="separator" class="divider"></li>
             <li><a href="dashboard.php?cat=web&page=logout"><span class="icon-off"></span> Keluar</a></li>
          </ul>
        </li>
      </ul> -->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
