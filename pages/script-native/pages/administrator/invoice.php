<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>Invoice</title>
	
	<link rel='stylesheet' type='text/css' href='css/style.css' />
	<link rel='stylesheet' type='text/css' href='css/print.css' media="print" />
	<script type='text/javascript' src='js/jquery-1.3.2.min.js'></script>
	<script type='text/javascript' src='js/example.js'></script>

</head>

<body>

	<div id="page-wrap">

		<textarea id="header">INVOICE</textarea>
		
		<div id="identity">
		
            <textarea id="address">Head Office :
Jl. Raya Lenteng Agung, No.129
Jakarta Selatan.
Jakarta - INDONESIA
			
Email : cso@dragonfly-erp.com
Telepon : (021) 574-9834</textarea>

            <div id="logo">

              <div id="logoctr">
                <a href="javascript:;" id="change-logo" title="Change logo">Ubah Logo</a>
                <a href="javascript:;" id="save-logo" title="Save changes">Simpan</a>
                |
                <a href="javascript:;" id="delete-logo" title="Delete logo">Ubah Logo</a>
                <a href="javascript:;" id="cancel-logo" title="Cancel changes">Batal</a>
              </div>

              <div id="logohelp">
                <input id="imageloc" type="text" size="50" value="" /><br />
                (max width: 540px, max height: 100px)
              </div>
              <img id="image" src="images/logo.png" alt="logo" />
            </div>
		
		</div>
		
		<div style="clear:both"></div>
		
		<div id="customer">

           <!--- <textarea id="customer-title">ABCDxxxxx
</textarea>---->

            <table id="meta">
                <tr>
                    <td class="meta-head">No. Invoice</td>
                    <td class="total-value"><textarea>000123</textarea></td>
                </tr>
                <tr>
                    <td class="meta-head">Tanggal</td>
                    <td class="total-value"><textarea id="date">December 15, 2009</textarea></td>
                </tr>
                <tr>
                    <td class="meta-head">Jumlah Tagihan</td>
                    <td><div class="due"> </div></td>
                </tr>

            </table>
		
		</div>
		
		<table id="items">
		
		  <tr>
		      <th>Item</th>
		      <th>Deskripsi</th>
		      <th>Harga Satuan</th>
		      <th>Quantity</th>
		      <th>Harga</th>
		  </tr>
		  
		  <tr class="item-row">
		      
		  </tr>
		  
		  <tr id="hiderow">
		    <td colspan="5"><a id="addrow" href="javascript:;" title="Add a row">Tambah Data</a></td>
		  </tr>
		  
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Subtotal</td>
		      <td class="total-value"><div id="subtotal">Rp.0</div></td>
		  </tr>
		  <tr>
		      <td colspan="2" class="blank"></td>
		      <td colspan="2" class="total-line">VAT</td>
		      <td class="total-value"><textarea id="paid">0</textarea></td>
		  </tr>
		  <tr>
		      <td colspan="2" class="blank"></td>
		      <td colspan="2" class="total-line">Total</td>
		      <td class="total-value"><div id="total">Rp.0</div></td>
		  </tr>
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line balance">Total Tagihan</td>
		      <td class="total-value balance"><div class="due">Rp.0</div></td>
		  </tr>
		
		</table>
		
		<div id="terms">
		  <h5>Ketentuan</h5>
		  <textarea>Bila Invoice Belum dibayarkan dalam waktu 30 hari, maka pihak kami akan menambahkan denda dengan inflasi 1.5%</textarea><br><br>
		</div>
	
	</div>
	
</body>

</html>