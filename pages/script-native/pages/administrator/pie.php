	<link rel="stylesheet" href="asset/jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="asset/scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="asset/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="asset/jqwidgets/jqxdraw.js"></script>
    <script type="text/javascript" src="asset/jqwidgets/jqxchart.core.js"></script>
    <script type="text/javascript" src="asset/jqwidgets/jqxdata.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // prepare chart data as an array
            var source =
            {
                datatype: "csv",
                datafields: [
                    { name: 'Browser' },
                    { name: 'Share' }
                ],
                url: 'asset/desktop.txt'
            };

            var dataAdapter = new $.jqx.dataAdapter(source, { async: false, autoBind: true, loadError: function (xhr, status, error) { alert('Error loading "' + source.url + '" : ' + error); } });

            // prepare jqxChart settings
            var settings = {
                //title: "Desktop browsers share",
                //description: "",
                enableAnimations: true,
                showLegend: false,
                showBorderLine: true,
                legendPosition: { left: 5, top: 5, width: 5, height: 5 },
                padding: { left: 5, top: 5, right: 5, bottom: 5 },
                titlePadding: { left: 0, top: 0, right: 0, bottom: 10 },
                source: dataAdapter,
                colorScheme: 'scheme02',
                seriesGroups:
                    [
                        {
                            type: 'pie',
                            showLabels: true,
                            series:
                                [
                                    { 
                                        dataField: 'Share',
                                        displayText: 'Browser',
                                        labelRadius: 90,
                                        initialAngle: 15,
                                        radius: 90,
                                        centerOffset: 0,
                                        formatSettings: { sufix: '%', decimalPlaces: 1 }
                                    }
                                ]
                        }
                    ]
            };

            // setup the chart
            $('#chartContainer').jqxChart(settings);
        });
    </script>

	<div id='chartContainer' style="width: 260px; height: 260px;">
	</div>

