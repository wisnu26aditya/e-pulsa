	<link rel="stylesheet" href="asset/jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="asset/scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="asset/scripts/demos.js"></script>
    <script type="text/javascript" src="asset/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="asset/jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Create a jqxMenu
            $("#jqxMenu").jqxMenu({ width: 1900, height: 32, showTopLevelArrows: true, keyboardNavigation: true });
            $("#jqxMenu").on('itemclick', function (event) {
                $("#log").html("Clicked: " + $(event.args).text());
            });
            $("#jqxMenu").css('visibility', 'visible');
            $("#jqxMenu").jqxMenu('focus');
        });
    </script>

	
    <div id='jqxMenu' style='visibility: hidden; margin-left: 0px;'>
        <ul>
            <li><a href="dashboard.php">Main Dashboard</a></li>
            <li>MENU 1
                        <ul style='width: 210px;'>
                            <li><a href="?cat=administrator&page=home">SUBMENU 1</a></li>
							<li><a href="?cat=administrator&page=home">SUBMENU 2</a></li>
                            <li type='separator'></li>
							<li><a href="?cat=administrator&page=home">SUBMENU 3</a></li>    
                        </ul>
            </li>
            
        </ul>
    </div>
    