<link rel="stylesheet" type="text/css" href="./fiturjquery/chosen/chosen.min.css" media="all" />
<script type="text/javascript" src="./fiturjquery/chosen/chosen.jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="./fiturjquery/datatables/css/dataTables.bootstrap.min.css" media="all" />
<script type="text/javascript" src="./fiturjquery/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="./fiturjquery/datatables/js/dataTables.bootstrap.min.js"></script>

<ol class="breadcrumb">
  <li><a href="dashboard.php">Home</a></li>
  <li>View Transaksi</li>
  <li><a href="dashboard.php?cat=hr&page=view">View</a></li>
</ol>

<?php
$query_trans= "	select 	a.trans_id,
							date_format(a.trans_tgl,'%d %M %Y') tgl,
							b.op_nama,
							a.trans_jml,
							a.trans_pembeli,
							a.trans_created_date
							from trans_ms a
							JOIN op_ms b ON a.trans_op=b.op_id
							ORDER BY a.trans_created_date asc";
$sql_trans = mysql_query($query_trans);

?>
<hr class="one" />
<h3>Data Transaksi E-Pulsa <a href="dashboard.php?cat=hr&page=transaksi" class="btn btn-xs btn-warning">Tambah</a></h3>

<table class="table table-bordered table-hover" id="myTable">
	<thead>
		<tr>
    <th><b>NO.</b></th>
    <th><b>NAMA PEMBELI</b></th>
    <th><b>TANGGAL PEMBELIAN</b></th>
    <th><b>OPERATOR</b></th>
    <th><b>JUMLAH PEMBELIAN</b></th>
		</tr>
	</thead>
	<tbody>
<?php
	$no = 1;
	while ($result = mysql_fetch_array($sql_trans)){
	$tgl = $result['tgl'];
	$day = date ('D', strtotime($tgl));
	$daylist = array (
						'Sun' => 'Minggu',
						'Mon' => 'Senin',
						'Tue' => 'Selasa',
						'Wed' => 'Rabu',
						'Thu' => 'Kamis',
						'Fri' => 'Jumat',
						'Sat' => 'Sabtu'
						);
    
?>
  <tr>
	<td><?php echo $no++ ?></td>
    <td><?php echo $result['trans_pembeli']; ?></td>
    <td><?php echo $daylist[$day].", ".$result['tgl']; ?></td>
    <td><?php echo $result['op_nama']; ?></td>
    <td><?php echo "Rp. ".number_format($result['trans_jml'],0,',','.'); ?></td>
  </tr>
<?php } ?>

</tbody>
</table>
<script type="text/javascript" language="javascript" >
			$(document).ready(function() {
			$('#myTable').DataTable();
			} );
</script>