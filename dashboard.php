<?php
date_default_timezone_set('Asia/Jakarta');
session_start();
if(!isset($_SESSION['login_hash']))
{
	echo "<script>window.location='index.php'</script>";
}
include("_db.php");
?>


<!DOCTYPE html>
<html lang="en-US">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>LOGIN USER</title>
	<?php include("_scr.php"); ?>
	<style type="text/css">
		.footer{
			border-top:1px solid #ddd;
			margin-top:40px;
		}
		.footer p {
			padding:15px 0px;
			margin:0 0 0 -5px;
		}
		.one{
			width:100%;
			border-top:1px solid #ddd;
		}
		.breadcumb{
			background:rgba(237,237,237,1);
			border-bottom: 1px solid #bbb;
			margin-top:-19px;
			margin-bottom:20px;
			padding:15px;
		}
		.breadcumb h1{
			font-size:14px;
			margin:0px;
			fonnt-style:bold;
		}
	</style>
</head>

<body onload="tampilkanwaktu();setInterval('tampilkanwaktu()', 1000);">
<?php include("menu.php"); ?>
 <div class="breadcumb">
 <table width="100%">
 <tr>
 	<td><h1>Aplikasi E-Pulsa <span align="right"></span></h1></td>
 	<td align="right"><?php include ("pages/web/timer.php") ?></td>
 </tr>
 </table>

 </div>
<div class="container-fluid">
	<?php
				$v_cat = (isset($_REQUEST['cat'])&& $_REQUEST['cat'] !=NULL)?$_REQUEST['cat']:'';
				$v_page = (isset($_REQUEST['page'])&& $_REQUEST['page'] !=NULL)?$_REQUEST['page']:'';
				if(file_exists("pages/".$v_cat."/".$v_page.".php"))
				{
					include("pages/".$v_cat."/".$v_page.".php");
				}else{
					include("pages/web/homepage.php");
				}
	?>
</div>		
  <?php include("_footer.php"); ?>
</body>
</html>
